<?php

// src/prostages/PropositionsBundle/DataFixtures/ORM/LoadStageData.php

namespace prostages\PropositionsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use prostages\PropositionsBundle\Entity\Entreprise;
use prostages\PropositionsBundle\Entity\Formation;
use prostages\PropositionsBundle\Entity\Stage;

/**
 * Load nos stages en base de données
 */

class Stages implements FixtureInterface
{

  public function load(ObjectManager $manager)
  {

     /* formations */

     $f1 = new Formation();
     $f1->setNom("DUT info");
     $manager->persist($f1);

     $f2 = new Formation();
     $f2->setNom("Licence pro multimédia");
     $manager->persist($f2);

     $f3 = new Formation();
     $f3->setNom("DU TIC");
     $manager->persist($f3);

     /* entreprises */

     $e1 = new Entreprise();
     $e1->setNom("Price Induction");
     $e1->setActivite("Aéronautique");
     $e1->setAdresse("Esplanade de l'Europe, 64600 Anglet");
     $e1->setSiteweb("http://www.price-induction.com/");
     $manager->persist($e1);

     $e2 = new Entreprise();
     $e2->setNom("Géomatika");
     $e2->setActivite("Développement d'applications autour des systèmes d'information géographiques");
     $e2->setAdresse("Créaticité - Bâtiment B - Technopole Izarbel, 64210 Bidart");
     $e2->setSiteweb("https://www.geomatika.fr/");
     $manager->persist($e2);

     $e3 = new Entreprise();
     $e3->setNom("Quiksilver");
     $e3->setActivite("Développement de produits pour sports de glisse");
     $e3->setAdresse("162, rue Belharra, 64500 Saint Jean De Luz");
     $e3->setSiteweb("http://www.quiksilver.fr/");
     $manager->persist($e3);

     $e4 = new Entreprise();
     $e4->setNom("Kapsicum");
     $e4->setActivite("Conseil web, création de sites e-commerce");
     $e4->setAdresse("26 hameau de la Cavette, 64230 Lescar");
     $e4->setSiteweb("http://www.kapsicum.fr/");
     $manager->persist($e4);

     /* stages */

     $s1 = new Stage();
     $s1->setNom("Refonte et mise à jour du site web de Price Induction");
     $s1->setMail("jc.bergouin@priceinduction.com");
     $s1->setMission("tude du site existant -Prise en compte des mises à jour à effectuer
     -Définition des différents rendus à intégrer (multimédia) -Définition de planning (jalons)
     -Réalisation des spécifications fonctionnelles du site + validation
     -Réalisation des spécifications techniques du site + validation -Choix du framework technique à utiliser
     -Refonte technique , développement -Tests -Présentation de la refonte à la direction
     -Corrections -Finalisation et déploiement en production.");
     $s1->addFormation($f1);
     $s1->setEntreprise($e1);
     $manager->persist($s1);

     $s2 = new Stage();
     $s2->setNom("Développement d'un viewer cartographique avec des technologies web et compatible
     avec l'ensemble des supports : PC, Tablettes type Android/Ipad et smartphones");
     $s2->setMail("pierre.dupont@geomatika.fr");
     $s2->setMission("Le stage se déroulera en trois étapes : - Étape 1 : Développement d'une application
     cartographique de consultation des données produites par la plateforme isigéo sous la forme d'un viewer
     simple (dans l'esprit de Google Maps) en utilisant les composants openlayers et jquery mobile, et compatible
     avec des Pcs et des navigateurs standards, mais aussi des tablettes sous Android ou des iphones ou encore des
     smartphones ( Android, Iphone .. ) - Étape 2 : Ajouter un bouton « Me connecter en tant que.. » pour permettre
     à tous les utilisateurs extranet de retrouver leurs cartes et leurs données ainsi que des fonctionnalités de
     consultations avancées - Étape 3 : Encapsuler le développement dans une application pour smartphone développé
     avec l'environnement PhoneGap. (Connexion de l'application de géo-tracking et de déclaration des incidents).
     Exploitation des capacités multimédias des tablettes (appareil photo, GPS...). Conception de charte graphique
     pour l'interface utilisateur de l'application (retouche photo, traitement d'image ...). Réalisation de vidéo
     pour la présentation de l'outil (ex : diffusion sur youtube). Le stage aura lieu en partenariat et collaboration
     étroit avec la DSI de la Ville de Bayonne (utilisation de leurs données et déplacements sur site à prévoir).");
     $s2->addFormation($f1);
     $s2->addFormation($f2);
     $s2->setEntreprise($e2);
     $manager->persist($s2);

     $s3 = new Stage();
     $s3->setNom("Assistant web developper");
     $s3->setMail("julien.giovani@quiksilver.fr");
     $s3->setMission("Intégré(e) à l'équipe du Marketing Digital, et rattaché(e) au pôle web, vous accompagnez notre
     équipe de développeurs dans l'ensemble de leurs tâches et interviendrez avec eux en support des projets innovants
     pour toutes les marques du groupe (Quiksilver, DC Shoes, Roxy ...). Votre mission consistera ainsi à les épauler
     dans la réalisation des développements de sites web basés sur les technologies PHP, Javascript, MySQL. Vous
     participerez également au développement de templates et d'extensions Back Office Métier ( Homerun eCommerce
     & Homerun PIM )et Front Office, au suivi et à la résolution des anomalies. Lien permanent avec l'équipe des
     graphistes et suivi des créas pour les projets alloués. Échange par rapport aux projets, expression des besoins
     graphiques et multimédias. Participation aux travaux graphiques et multimédias,
     conception/réalisation d'animations Flash, de chartes graphiques, ... Les réalisations multimédias se feront en
     fonction des besoins au moment du stage.");
     $s3->addFormation($f1);
     $s3->setEntreprise($e3);
     $manager->persist($s3);

     $s4 = new Stage();
     $s4->setNom("Étude ergonomique de pages utilisateurs clients et administrateurs pour un site de billetterie en ligne.");
     $s4->setMail("contact-stage@kapiscum.fr");
     $s4->setMission("L'étude comprend l'analyse fonctionnelle des pages , la préparation graphique, puis l'intégration en html/css.
     Les activités principales menées durant le stage sont les suivantes : Étude initiale, prise en compte de l'existant, évaluation
     des besoins réels. - Dossier d'étude fonctionnelle : A- Préparation graphique 1 B- Présentation charte graphique 1
     C- Intégration des pages web charte 1 - Reprise cycle ABC, autant de fois que le temps le permet... - Livraison des chartes.
     - Préparation soutenance. Travail seul sur le développement sous la tutelle de l'équipe projet, support technique et
     graphique apporté par le tuteur.");
     $s4->addFormation($f3);
     $s4->setEntreprise($e4);
     $manager->persist($s4);

     $s5 = new Stage();
     $s5->setNom("Assistant web designer");
     $s5->setMail("julien.giovani@quiksilver.fr");
     $s5->setMission("Intégré(e) à l'équipe du Marketing Digital, et rattaché(e) au pôle web, vous accompagnez notre
     équipe de développeurs dans l'ensemble de leurs tâches et interviendrez avec eux en support des projets innovants
     pour toutes les marques du groupe (Quiksilver, DC Shoes, Roxy ...). Votre mission consistera ainsi à les épauler
     dans la réalisation des développements de sites web basés sur les technologies PHP, Javascript, MySQL. Vous
     participerez également au développement de templates et d'extensions Back Office Métier ( Homerun eCommerce
     & Homerun PIM )et Front Office, au suivi et à la résolution des anomalies. Lien permanent avec l'équipe des
     graphistes et suivi des créas pour les projets alloués. Échange par rapport aux projets, expression des besoins
     graphiques et multimédias. Participation aux travaux graphiques et multimédias,
     conception/réalisation d'animations Flash, de chartes graphiques, ... Les réalisations multimédias se feront en
     fonction des besoins au moment du stage.");
     $s5->addFormation($f1);
     $s5->setEntreprise($e3);
     $manager->persist($s5);

     $manager->flush();
   }
 }
