<?php

namespace prostages\PropositionsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Entreprise
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="prostages\PropositionsBundle\Entity\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100)
     * @Assert\NotBlank(message = "Le nom de l'entreprise ne doit pas être vide")
     * @Assert\Length(
     *  min = 4,
     *  minMessage = "Le nom de l'entreprise doit être composé d'au moins {{ limit }} caractères"
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="activite", type="string", length=255)
     * @Assert\NotBlank(message = "L'activité de l'entreprise ne doit pas être vide")
     */
    private $activite;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=100)
     * @Assert\NotBlank(message = "L'adresse de l'entreprise ne doit pas être vide")
     *
     * @Assert\Regex(
     *     pattern = "/ rue|boulevard|impasse|allée|place|route|voie /",
     *     message = "L'adresse doit comporter un type de voie valide"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/ \d{5} /",
     *     message = "L'adresse doit comporter un code postal valide"
     * )
     *
     * @Assert\Regex(
     *     pattern = "/^[1-9][0-9]{0,2}(bis| bis)? /",
     *     message = "L'adresse doit comporter un numéro de rue valide"
     * )
     * @Assert\Regex(
     *     pattern = "/^[1-9][0-9]{0,2}(bis| bis)? (rue|boulevard|impasse|allée|place|route|voie) [a-zA-Z ]+ \d{5} \w*$/",
     *     message = "L'adresse doit être valide"
     * )
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="siteweb", type="string", length=100)
     * @Assert\NotBlank(message = "Le site web de l'entreprise ne doit pas être vide")
     * @Assert\Url(message = "Le site web de l'entreprise doit être un url valide")
     */
    private $siteweb;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Entreprise
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set activite
     *
     * @param string $activite
     * @return Entreprise
     */
    public function setActivite($activite)
    {
        $this->activite = $activite;

        return $this;
    }

    /**
     * Get activite
     *
     * @return string
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Entreprise
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set siteweb
     *
     * @param string $siteweb
     * @return Entreprise
     */
    public function setSiteweb($siteweb)
    {
        $this->siteweb = $siteweb;

        return $this;
    }

    /**
     * Get siteweb
     *
     * @return string
     */
    public function getSiteweb()
    {
        return $this->siteweb;
    }
}
